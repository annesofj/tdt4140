# Gruppe 44 - Watchlist
Dette prosjektet er utviklet i samarbeid med produkteier Watchlist AS. Produkteier ønsket en
web-basert sosial plattform der brukere kan finne og anbefale
filmer og serier til følgere, og selv få anbefalinger fra de man følger.
Løsningen er inspirert av [Goodreads](https://www.goodreads.com)
og lar en bruker opprette en profil, noe som kreves for å kunne logge inn på nettsiden.

Som bruker kan man opprette filmlister, med en tilhørende rating til hver film i listen.
Det er også mulig å redigere og slette filmer fra listen. 
Brukere kan søke etter og følge andre brukere, *watchers*, for å se deres filmlister. 
På tilsvarende måte som filmlisten, er det også mulighet for å redigere følgerlisten sin og slutte å følge watchers.
Etterhvert som watchers man følger legger til nye filmer i sine filmlister, vil dette 
komme opp som oppdateringer i ens feed. 

En dedikert admin-bruker muliggjør administrering av nettsidens tilhørende database. 
Admin-brukeren kan redigere og slette brukerprofiler eller annet innhold som bryter med
nettsidens grunnleggende etiske retningslinjer.

## Motivasjon 
Produktet er et resultat av et utviklingsprosjekt gjennomført forbindelse med NTNU-emnet TDT4140: Programvareutvikling. 
Prosjektet gikk ut på at gruppen, i samråd med produkteier, skulle utvikle et produkt som oppfylte produkteiers problembeskrivelse. 
Gjennom kontinuerlig dialog med produkteier, ble ønsket funksjonalitet kartlagt og deretter implementert. 

## Team
* [Anne-Sofie Johansen](#annesofj@stud.ntnu.no)
* [Cornelius Hjort](#cornellh@stud.ntnu.no)
* [Inger Yri](#ingerhy@stud.ntnu.no)
* [Jens Maeland](#jensma@stud.ntnu.no)
* [Mathilde Marie Solberg](#mathimso@stud.ntnu.no)
* [Thomas Haugan](#thomas.haugan@ntnu.no)

## Kodekonvensjon
Gruppen har benyttet [Googles kodekonvensjon for JavaScript](https://google.github.io/styleguide/jsguide.html).

## Screenshots
**Figur 1**: Logo<br/>
<img src="doc/logo.png" width="500"><br/>
Se denne [wikien](https://gitlab.stud.idi.ntnu.no/tdt4140-2020/44/-/wikis/Brukermanual) for screenshots og brukermanual. <br/>


## Teknologier og rammeverk

### Frontend
[React](https://reactjs.org): Javascript-biblioteket benyttet i frontend <br/>
[JavaScript](https://www.javascript.com): Programmeringsspråk benyttet i frontend 

Systemet er bygget med følgende versjoner:
- React v16.12.0
- React Dom v16.12.0
- React Router Dom v16.12.0


### Backend
[Python](https://www.python.org/): Programmeringsspråket benyttet i backend<br/>
[Django](https://www.djangoproject.com/): Web-rammeverk skrevet i Python og benyttet i backend <br/>
[SQLite](https://www.sqlite.org/index.html): Benyttet Relational Database Management System(RDBMS), medfølger som standard DBMS i Django

Systemet er bygget med følgende versjoner:
- Python v3.8.0
- pip v20.0.2
- Django v3.0.3
- Django Cors Headers v3.2.1
- Django Rest Framework v3.11.0
- SQLite v3.0



## Installasjon og bruk

### Forutsetninger
Gruppen har benyttet Visual Studio Code, som kan lastes ned [her](https://code.visualstudio.com/download). 
Det er dog ikke et krav å benytte denne, så en øvrig editor som støtter utviklig i Python og JavaScript kan benyttes etter ønske. 

Det kreves derimot at man har Python 3.8.0 eller nyere, installert. <br/>
**Python** kan lastes ned [her](https://www.python.org/downloads/). <br/>
 - Når du installerer Python vil **pip** installeres automatisk for Windows og OS X, men må derimot installeres separat på Linux. <br/>
    - For å installere pip3 på Ubuntu eller Debian Linux, skriv følgende i terminalen: <br/> 
    `sudo apt-get install python3-pip` <br/>
    - For å installere pip3 på Fedora Linux, skriv følgende i terminalen: <br/> 
    `sudo yum install python3-pip` <br/>

**OBS:** Skriv "pip" i alle pip-kommandoer om du bruker Windows, eller "pip3" for alle pip-kommenadoer om du bruker Linux og OS X.<br/>

Videre må **Node.js** og **npm** installeres om man ikke har det fra før. <br/>
Node.js kan lastes ned [her](https://nodejs.org/en/download/). <br/>
Npm, som håndterer nedlasting og installering av JavaScript-pakker, vil
automatisk installeres sammen med Node.js. 


----

### Første gang prosjektet kjøres
*Installasjon av nødvendige rammeverk og igangkjøring av systemet gjøres i dette
prosjektet ved bruk av manuelle kommandoer i terminalen. Annotasjonen<br/>
`This is a command`<br/>
refererer til utførelse av kommandoer i terminal. Hvis ikke annet oppgis, er kommandoene like for Windows og Linux/OS X.*

#### Backend
1. Åpne et nytt vindu i terminalen og naviger deg til desktop/skrivebordet ditt: <br/>
`cd desktop` <br/>
og klon deretter git-repositoryet: <br/>
`git clone https://gitlab.stud.idi.ntnu.no/tdt4140-2020/44.git` <br/>
Hvis du er usikker på hvordan dette gjøres, kan du følge denne mer detaljerte
[guiden](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html).

2. Nå skal vi installere backend-delen av prosjektet. Først installerer vi alle
backend-dependencies. I repoet finnes tekstfilen requirements.txt som
spesifiserer alle avhengigheter (moduler) som trengs for å starte backend.
Åpne et nytt vindu i terminal, og naviger deg inn i mappen "44" 
hvor filen requirements.txt ligger. Ettersom du har lagret mappen "44" på
skrivebordet ditt, kan dette gjøres gjennom kommandoene: <br/>
`cd desktop` <br/>
`cd 44` <br/>
Nå som du er inne i mappen, skriv følgende i terminalen: <br/>
*Windows:* `pip install -r requirements.txt --user` <br/>
*Linux/OS X:*  `pip3 install -r requirements.txt` <br/>

3. Backend databasen må kjøres i et [Python Virtual Environment](https://realpython.com/python-virtual-environments-a-primer/), 
og et ferdig konfigurert virtuelt Python-miljø medfølger prosjektet. Naviger deg inn
i mappen "watchlist": <br/>
`cd watchlist` <br/>
Deretter kan du aktivere Python venv ved å skrive: <br/>
*Windows:* `.\venv_win\scripts\activate.bat` <br/>
*Linux/OS X:* `source env/bin/activate`<br/>
Det skal nå stå “(env)” helt til venstre på linjen du er på i terminalen.

4. Nå er vi klare til å starte opp backend. Gå et hakk ut i filstrukturen (så du kommer tilbake til "44"-mappen) 
ved å skrive følgende i terminalen: <br/>
*Windows:* `cd..` <br/>
*Linux/OS X:* `cd ..` <br/>
Nå skal du være tilbake i mappen "44", hvor filen "manage.py" ligger. For å starte serveren, skriv følgende i terminal: <br/>
*Windows:* `python manage.py runserver` <br/>
*Linux/OS X:*`python3 manage.py runserver` <br/>
Da bør følgende komme opp i terminalen din: <br/>
<img src="doc/launc_backend.png" width="700"><br/>

5. Nå kjører serveren på port 8000 på din pc(http://127.0.0.1:8000/). For å aksessere superbruker, 
åpne nettleser og benytt URL: http://127.0.0.1:8000/admin <br/>
Brukernavn: "admin", Passord: "qweqwe". <br/>
<img src="doc/fullscreenAdminView.png" width="700"> <br/>
	
#### Frontend 
1. Nå som backend er oppe og kjører, skal vi få opp frontend. Åpne en ny tab i terminalen din, ved å trykke: <br/>
*Windows:* ctrl + Win + T <br/>
*Linux:* ctrl + alt + T <br/>
*OS X:* cmd + T <br/>
Naviger deg inn i mappen "44", hvis du ikke allerede er der, slik: <br/>
`cd desktop` <br/>
`cd 44` <br/>
Deretter navigerer du deg inn i mappen “frontend” slik: <br/>
`cd frontend` <br/>

2. Nå er det tid for å installere alle avhengigheter i frontend-delen av prosjektet. Alle avhengighetene er listet opp i 
filen package.json, og installeres enkelt ved å skrive følgende i terminalen: <br/>
`npm install` <br/>

3. Når denne prosessen er gjennomført, undersøk om det kommer vulnerabilities. Om dette er tilfellet, skriv følgende i terminalen: <br/>
`npm audit fix` <br/>
Nå skal alt fungere, også selv om det er noen vulnerabilities som ikke har blitt løst. 

4. Nå er vi klare til å starte opp frontend-delen av prosjektet. Mens du er i mappen "frontend" skriv: <br/>
`npm start` <br/>
Nå skal frontend-delen automatisk åpnes i din nettleser, hvis dette ikke skjer, kan du finne den på URL http://localhost:3000. 

5.  Watchlist-applikasjonen skal nå fungere i sin helhet, og du kan benytte deg av produktets funksjonalitet. 
Funksjonalitet kan du forøvirg lese mer om i [brukermanualen](https://gitlab.stud.idi.ntnu.no/tdt4140-2020/44/-/wikis/Brukermanual).


*Dersom du ser at noe ikke fungerer som det skal etter denne installasjonen,
kan du henvende deg til Jens Maeland på jensma@stud.ntnu.no*


----

### Oppstart av prosjektet fra andre gang og utover
1. Åpne terminalen din og naviger deg inn i mappen "watchlist": <br/>
`cd desktop` <br/>
`cd 44` <br/>
Deretter navigerer du deg inn i mappen “frontend” slik: <br/>
`cd watchlist` <br/>


2. Når du er i mappen “watchlist”, skriv følgende i terminal for å starte opp venv: <br/>
*Windows:* `.\venv_win\scripts\activate.bat` <br/>
*Linux/OS X:* `source env/bin/activate` <br/>
Da skal det stå “(env)” helt til venstre på linjen du er på i terminalen.

3. Naviger tilbake til "44"-mappen ved å skrive: <br/>
*Windows:* `cd..` <br/>
*Linux/OS X:* `cd ..` <br/>
Skriv deretter: <br/>
*Windows:* `python manage.py runserver` <br/>
*Linux/OS X:*`python3 manage.py runserver` <br/>
Nå er backend oppe og kjøre på port 8000(http://127.0.0.1:8000/).

4. Åpne en ny tab i terminalen din, ved å trykke: <br/>
*Windows:* ctrl + Win + T <br/>
*Linux:* ctrl + alt + T <br/>
*OS X:* cmd + T <br/>
Naviger deg inn i mappen "44", hvis du ikke allerede er der, slik: <br/>
`cd desktop` <br/>
`cd 44` <br/>
Deretter navigerer du deg inn i mappen “frontend” slik: <br/>
`cd frontend` <br/>


5. Skriv følgende i terminal for å starte opp frontend: <br/>
`npm start` <br/>
Nå skal frontend-delen automatisk åpnes i din nettleser, hvis dette ikke skjer, kan du finne den på URL http://localhost:3000. 

6.  Watchlist-applikasjonen skal nå fungere i sin helhet, og du kan benytte deg av produktets funksjonalitet. 
Funksjonalitet kan du forøvirg lese mer om i [brukermanualen](https://gitlab.stud.idi.ntnu.no/tdt4140-2020/44/-/wikis/Brukermanual).


## Testing
Dette prosjektet har kun benyttet seg av automatiske enhetstester for backend. Mer om testing av produktet kan finnes på denne [wikien](https://gitlab.stud.iie.ntnu.no/tdt4140-2020/44/-/wikis/Oversikt-over-kodekvalitet).
Testene kjøres ved å navigere seg inn i mappen "44" i terminalen:<br/>
Naviger deg inn i mappen "44", hvis du ikke allerede er der, slik: <br/>
*Windows:* `cd desktop\44` <br/>
*Linux/OS X:* `cd desktop/44`<br/>
og deretter skrive: <br/>
*Windows:* `python manage.py test` <br/>
*Linux/OS X:* `python3 manage.py test`

## **Instruksjonsvideoer**
**Klone prosjektet fra GitLab**<br/>
![](https://gitlab.stud.idi.ntnu.no/tdt4140-2020/44/-/raw/master/doc/clone_repo.gif)<br/>

**Installere backend dependencies**<br/>
![](https://gitlab.stud.idi.ntnu.no/tdt4140-2020/44/-/raw/master/doc/pip3_and_dependencies.gif)<br/>

**Oppstart backend server**<br/>
![](https://gitlab.stud.idi.ntnu.no/tdt4140-2020/44/-/raw/master/doc/boot_backend.gif)<br/>

**Oppstart frontend**<br/>
![](https://gitlab.stud.idi.ntnu.no/tdt4140-2020/44/-/raw/master/doc/boot_frontend.gif)<br/>



