from django.test import TestCase
from rest_framework.test import APITestCase
from .models import Followers, Watchlist, CustomUser, Feedelement
from rest_framework.authtoken.models import Token

# Run "python3 manage.py test" from terminal to run tests


# ------------------------------ Model tests ------------------------------

# Testing correct creation of CustomUsers
class CustomUserTestCase(TestCase):  
    def setUp(self):
        CustomUser.objects.create(
            username="testcase1", email="testcase1@localhost.com", password="strong_psw1")
        CustomUser.objects.create(
            username="testcase2", email="testcase2@localhost.com", password="strong_psw2")

    def test_user_info(self):
        queryset = CustomUser.objects.all()
        self.assertEqual(queryset.count(), 2)
        user1 = CustomUser.objects.get(username="testcase1")
        self.assertEqual(user1.username, "testcase1")
        self.assertEqual(user1.email, "testcase1@localhost.com")
        self.assertEqual(user1.password, "strong_psw1")
        self.assertEqual(user1.id, 1)

# Testing correct creation of Watchlists
class WatchlistTestCase(TestCase):  
    def setUp(self):
        CustomUser.objects.create(
            username="testcase1", email="testcase1@localhost.com", password="strong_psw1")
        CustomUser.objects.create(
            username="testcase2", email="testcase2@localhost.com", password="strong_psw2")
        CustomUser.objects.create(
            username="testcase3", email="testcase3@localhost.com", password="strong_psw3")
        user1 = CustomUser.objects.get(username="testcase1")
        user2 = CustomUser.objects.get(username="testcase2")
        user3 = CustomUser.objects.get(username="testcase3")
        Watchlist.objects.create(user=user1, movies="movieA-4,movieB-1")
        Watchlist.objects.create(
            user=user2, movies="movieB-3,movieC-5,movieD-4")
        Watchlist.objects.create(user=user3, movies="movieE-2")

    def test_watchlist_info(self):
        queryset = CustomUser.objects.all()
        self.assertEqual(queryset.count(), 3)
        user = CustomUser.objects.get(username="testcase2")
        watchlist = Watchlist.objects.get(user=user)
        self.assertEqual(watchlist.user, user)
        self.assertEqual(watchlist.movies, "movieB-3,movieC-5,movieD-4")

# Testing correct creation of Followers
class FollowersTestCase(TestCase):  
    def setUp(self):
        CustomUser.objects.create(
            username="testcase1", email="testcase1@localhost.com", password="strong_psw1")
        CustomUser.objects.create(
            username="testcase2", email="testcase2@localhost.com", password="strong_psw2")
        CustomUser.objects.create(
            username="testcase3", email="testcase3@localhost.com", password="strong_psw3")
        user1 = CustomUser.objects.get(username="testcase1")
        user2 = CustomUser.objects.get(username="testcase2")
        user3 = CustomUser.objects.get(username="testcase3")

        Followers.objects.create(
            user=user1, followers="testcase2:testcase2,testcase3")
        Followers.objects.create(
            user=user2, followers="testcase1:testcase1")
        Followers.objects.create(
            user=user3, followers=":testcase1")

    def test_followers_info(self):
        queryset = Followers.objects.all()
        self.assertEqual(queryset.count(), 3)
        user = CustomUser.objects.get(username="testcase1")
        followers = Followers.objects.get(user=user)
        self.assertEqual(followers.user, user)
        self.assertEqual(followers.followers, "testcase2:testcase2,testcase3")

# Testing correct creation of Feedelements
class FeedelementTestCase(TestCase):
    def setUp(self):
        CustomUser.objects.create(
            username="testcase1", email="testcase1@localhost.com", password="strong_psw1")
        CustomUser.objects.create(
            username="testcase2", email="testcase2@localhost.com", password="strong_psw2")
        user1 = CustomUser.objects.get(username="testcase1")
        user2 = CustomUser.objects.get(username="testcase2")
        Feedelement.objects.create(
            username=user1.username, addedMovie="movieA-4")
        Feedelement.objects.create(
            username=user1.username, addedMovie="movieB-1")
        Feedelement.objects.create(
            username=user2.username, addedMovie="movieB-3")
        Feedelement.objects.create(
            username=user2.username, addedMovie="movieC-5")
        Feedelement.objects.create(
            username=user2.username, addedMovie="movieD-4")

    def test_feed_info(self):
        queryset = Feedelement.objects.all()
        self.assertEqual(queryset.count(), 5)
        user = CustomUser.objects.get(username="testcase1")
        liste = Feedelement.objects.filter(username=user.username)
        self.assertEqual(liste[0].username, user.username)
        self.assertEqual(liste[0].addedMovie, "movieA-4")
        self.assertEqual(liste[1].username, user.username)
        self.assertEqual(liste[1].addedMovie, "movieB-1")


# ------------------------------ API tests ------------------------------

# Testing get, post and delete of CustomUsers
class CostumUserAPITestCase(APITestCase):
    def setUp(self):
        CustomUser.objects.create(
            username="testwatcher", email="testwatcher@localhost.com", password="strong_psw")

    def test_get_method(self):
        user = CustomUser.objects.get(username="testwatcher")
        authtoken = Token.objects.get(user=user)
        url = "http://127.0.0.1:8000/users/"
        headers = {
            'HTTP_AUTHORIZATION': 'Token ' + str(authtoken)
        }

        # Checking that unauthorized users are not allowed
        unauth_response = self.client.get(url)
        self.assertEqual(unauth_response.status_code, 403)

        # Checking that authorized users are allowed
        response = self.client.get(url, **headers)
        self.assertEqual(response.status_code, 200)
        queryset = CustomUser.objects.all()
        self.assertEqual(queryset.count(), 1)

    def test_post_method(self):
        user = CustomUser.objects.get(username="testwatcher")
        authtoken = Token.objects.get(user=user)
        url = "http://127.0.0.1:8000/users/"
        headers = {
            'HTTP_AUTHORIZATION': 'Token ' + str(authtoken)
        }
        data = {
            "username": "testwatcher2",
            "email": "testwatcher2@localhost.com",
            "password": "strong_psw2"
        }

        unauth_response = self.client.get(url)
        self.assertEqual(unauth_response.status_code, 403)

        response = self.client.post(url, data, format="json", **headers)
        self.assertEqual(response.status_code, 201)

    def test_delete_method_success(self):
        user = CustomUser.objects.get(username="testwatcher")
        authtoken = Token.objects.get(user=user)

        url = "http://127.0.0.1:8000/users/"+str(user.id)+"/"
        headers = {
            'HTTP_AUTHORIZATION': 'Token ' + str(authtoken)
        }

        unauth_response = self.client.get(url)
        self.assertEqual(unauth_response.status_code, 403)

        response = self.client.delete(url, **headers)
        self.assertEqual(response.status_code, 204)


# Testing get, post and delete of Watchlists
class WatchlistAPITestCase(APITestCase):
    def setUp(self):
        CustomUser.objects.create(
            username="testwatcher", email="testwatcher@localhost.com", password="strong_psw")
        CustomUser.objects.create(
            username="testwatcher2", email="testwatcher2@localhost.com", password="strong_psw2")
        user = CustomUser.objects.get(username="testwatcher")
        user = CustomUser.objects.get(username="testwatcher")
        Watchlist.objects.create(
            user=user, movies="Movie A-2,Movie B-3,Movie C-5")

    def test_get_method(self):
        user = CustomUser.objects.get(username="testwatcher")
        authtoken = Token.objects.get(user=user)
        url = "http://127.0.0.1:8000/watchlist/"
        headers = {
            'HTTP_AUTHORIZATION': 'Token ' + str(authtoken)
        }
        unauth_response = self.client.get(url)
        self.assertEqual(unauth_response.status_code, 403)

        response = self.client.get(url, **headers)
        self.assertEqual(response.status_code, 200)

    def test_post_method(self):
        user = CustomUser.objects.get(username="testwatcher")
        authtoken = Token.objects.get(user=user)
        url = "http://127.0.0.1:8000/watchlist/"
        headers = {
            'HTTP_AUTHORIZATION': 'Token ' + str(authtoken)
        }
        data = {
            "user": "2",
            "movies": "Movie D-5,Movie E-5"
        }
        unauth_response = self.client.get(url)
        self.assertEqual(unauth_response.status_code, 403)
        response = self.client.post(url, data, format="json", **headers)
        self.assertEqual(response.status_code, 201)

    def test_delete_method_success(self):
        user = CustomUser.objects.get(username="testwatcher")
        authtoken = Token.objects.get(user=user)

        url = "http://127.0.0.1:8000/watchlist/"+str(user.id)+"/"
        headers = {
            'HTTP_AUTHORIZATION': 'Token ' + str(authtoken)
        }
        unauth_response = self.client.get(url)
        self.assertEqual(unauth_response.status_code, 403)
        response = self.client.delete(url, **headers)
        self.assertEqual(response.status_code, 204)


# Testing get, post and delete of Followers
class FollowersAPITestCase(APITestCase):
    def setUp(self):
        CustomUser.objects.create(
            username="testwatcher", email="testwatcher@localhost.com", password="strong_psw")
        CustomUser.objects.create(
            username="testwatcher3", email="testwatche3r@localhost.com", password="strong_psw3")
        user = CustomUser.objects.get(username="testwatcher")
        Followers.objects.create(
            user=user, followers="testwatcher2:testwatcher2")

    def test_get_method(self):
        url = "http://127.0.0.1:8000/followers/"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_post_method(self):
        url = "http://127.0.0.1:8000/followers/"
        data = {
            'user': "2",
            'followers': "testwatcher:"
        }
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, 201)

    def test_delete_method_success(self):
        user = CustomUser.objects.get(username="testwatcher")
        url = "http://127.0.0.1:8000/followers/"+str(user.id)+"/"
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 204)


# Testing get, post and delete of Feedelements
class FeedelementAPITestCase(APITestCase):
    def setUp(self):
        CustomUser.objects.create(
            username="testwatcher", email="testwatcher@localhost.com", password="strong_psw")
        user = CustomUser.objects.get(username="testwatcher")
        Feedelement.objects.create(
            username=user.username, addedMovie="Movie B-3")
        Feedelement.objects.create(
            username=user.username, addedMovie="Movie C-5")

    def test_get_method(self):
        url = "http://127.0.0.1:8000/feedelement/"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_post_method(self):
        url = "http://127.0.0.1:8000/feedelement/"
        data = {
            'username': "testwatcher2",
            'addedMovie': "Movie F-2"
        }
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, 201)

    def test_delete_method_success(self):
        url = "http://127.0.0.1:8000/feedelement/"+str(1)+"/"
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 204)
