from django.contrib import admin

from .models import Followers, Watchlist, CustomUser, Feedelement  # Movie

'''
class MovieAdmin(admin.ModelAdmin):
    list_display = ('title', 'rating')
'''

# ------------------------- Admin access to the different DB tables -------------------------


class FollowersAdmin(admin.ModelAdmin):
    list_display = ('user', 'followers')


class FeedelementAdmin(admin.ModelAdmin):
    list_display = ('username', 'addedMovie')


class WatchlistAdmin(admin.ModelAdmin):
    list_display = ('user', 'movies')


class UserAdmin(admin.ModelAdmin):
    list_display = ('username', 'email')


#admin.site.register(Movie, MovieAdmin)
admin.site.register(Followers, FollowersAdmin)
admin.site.register(Feedelement, FeedelementAdmin)
admin.site.register(Watchlist, WatchlistAdmin)
admin.site.register(CustomUser, UserAdmin)
