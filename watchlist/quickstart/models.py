from django.db import models
from django.contrib.auth.models import AbstractUser
from django.dispatch import receiver
from django.conf import settings

from django.db.models.signals import post_save
from rest_framework.authtoken.models import Token


# ------------------------- Creating models for the different DB tables -------------------------

# For users, includes username, email, password
class CustomUser(AbstractUser):
    @receiver(post_save, sender=settings.AUTH_USER_MODEL)
    def create_auth_token(sender, instance=None, created=False, **kwargs):
        if created:
            Token.objects.create(user=instance)


'''  For futher development in third sprint, making movies into their own objects
class Movie(models.Model):
    title = models.CharField(max_length=50)
    rating = models.IntegerField()
'''


# Every users follow/following relations on the format (The user's followers):(who their following)
class Followers(models.Model):
    user = models.ForeignKey(
        to=CustomUser, primary_key=True, on_delete=models.CASCADE,)
    followers = models.CharField(
        max_length=30000, blank=True, null=True)


# Every users Watchlist(filmlist) on the format film-rating;film-rating;film-rating...
class Watchlist(models.Model):
    user = models.ForeignKey(
        to=CustomUser, primary_key=True, on_delete=models.CASCADE)
    movies = models.CharField(max_length=30000, blank=True, null=True)


# Everytime a user adds a film to their Watchlist(filmlist), a feedelement is created with username and movie-rating
class Feedelement(models.Model):
    username = models.CharField(max_length=50, blank=True, null=True)
    addedMovie = models.CharField(max_length=100, blank=True, null=True)
