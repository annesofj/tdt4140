from django.contrib.auth.models import User
from rest_framework import serializers
from .models import Followers, Watchlist, CustomUser, Feedelement  # Movie


# ------------------------- Creating serializers for the models -------------------------

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CustomUser
        fields = ('id', 'username', 'email', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    # How to create a new CutomUser object
    def create(self, validated_data):
        user = CustomUser(
            username=validated_data['username'],
            email=validated_data['email'],
        )
        user.set_password(validated_data['password'])
        user.save()
        return user


'''
class MovieSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Movie
        fields = ('title', 'duration', 'release')
'''


class FollowersSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Followers
        fields = ('user', 'followers')

    # How to create a new Followers object
    def create(self, validated_data):
        userid = int(validated_data['user'])
        newuser = CustomUser.objects.get(id=userid)

        follower = Followers(
            user=newuser,
            followers=validated_data['followers']
        )
        follower.save()
        return follower


class WatchlistSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Watchlist
        fields = ('user', 'movies')

    # How to create a new Watchlist object
    def create(self, validated_data):
        userid = int(validated_data['user'])
        newuser = CustomUser.objects.get(id=userid)

        watcher = Watchlist(
            user=newuser,
            movies=validated_data['movies']
        )
        watcher.save()
        return watcher


class FeedelementSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Feedelement
        fields = ('username', 'addedMovie')

    # How to create a new Feedelement object
    def create(self, validated_data):
        feedel = Feedelement(
            username=validated_data['username'],
            addedMovie=validated_data['addedMovie']
        )
        feedel.save()
        return feedel
