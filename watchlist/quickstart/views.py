from django.shortcuts import render
from rest_framework import viewsets, status
from .serializers import UserSerializer, FollowersSerializer, WatchlistSerializer, FeedelementSerializer
from .models import Followers, Watchlist, CustomUser, Feedelement

from rest_framework.response import Response
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny


# Register a new user
class RegisterView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer

# Send in username and password, get token


class LoginView(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        response = super(LoginView, self).post(request, *args, **kwargs)
        token = Token.objects.get(key=response.data['token'])
        return Response({'token': token.key})


# Send in token, get out userdata
class GetMeView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication, SessionAuthentication)

    def list(self, request):
        user = CustomUser.objects.get(username=request.user.username)
        return Response({'id': user.id, 'username': user.username, 'email': user.email})


# List out all films a user in their watchlist
class WatchlistView(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Watchlist.objects.all()
    serializer_class = WatchlistSerializer


# List out all following:followers of a user
class FollowersView(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Followers.objects.all()
    serializer_class = FollowersSerializer


# List out all feedelements
class FeedelementView(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Feedelement.objects.all()
    serializer_class = FeedelementSerializer
