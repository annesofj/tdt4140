from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from rest_framework import routers
from .quickstart import views

from rest_framework.authtoken import views as auth_views

router = routers.DefaultRouter()
router.register('users', views.RegisterView)
router.register('watchlist', views.WatchlistView)
router.register('followers', views.FollowersView)
router.register('feedelement', views.FeedelementView)
router.register('auth', views.GetMeView, basename='CustomUser')

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url('', include(router.urls)),
    url('admin/', admin.site.urls),
    url('login/', views.LoginView.as_view()),
]
