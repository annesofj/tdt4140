import React, { Component } from "react";
import { Link } from "react-router-dom";
import { getUser } from "./userAuth";

class FollowerComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: "",
            currentlyWatching: "None",
            profilePic: "./defaultProfilePic.png",
            allFollowerslist: [],
            listedFollowers: [],
            listedFollowing: [],
            numberOfFilms: 0,
            numberOfFollowers: 0,
            numberOfFollowing: 0
        };
    }

    async componentDidMount() {

        //Get all watchlists
        const headers = new Headers();
        headers.append("Content-Type", "application/json");
        headers.append("Authorization", `Token ${localStorage.getItem("token")}`);

        var parameters = {
            method: "GET",
            headers: headers,
            redirect: "follow"
        };

        const response = await fetch(
            "http://localhost:8000/watchlist/",
            parameters
        );
        const data = await response.json();

        let liste = [];
        data.map(obj => {
            liste.push(obj);
        });

        // Get this users userdata
        const token = localStorage.getItem("token");
        const userdata = await getUser(token);
        this.setState({ user: userdata });

        //Find this user in the watchlist and set the number of films accordingly
        for (const el of liste) {
            if (el.user === userdata.id) {
                this.setState({ listedMovies: el.movies });
                if (el.movies === null) {
                    this.setState({ numberOfFilms: 0 });
                } else {
                    const totalFilms = el.movies.split(";");
                    this.setState({ numberOfFilms: totalFilms.length });
                }
            }
        }

        //Get all followers:followings 
        const headers2 = new Headers();
        headers.append("Content-Type", "application/json");
        headers.append("Authorization", `Token ${localStorage.getItem("token")}`);

        var parameters = {
            method: "GET",
            headers: headers2,
            redirect: "follow"
        };

        const response2 = await fetch(
            "http://localhost:8000/followers/",
            parameters
        );
        const followingdata = await response2.json();

        // Add all users follower:followings to followinglist, and find this users followers:following
        let followinglist = [];
        let correctlist = ""
        followingdata.map(obj => {
            followinglist.push(obj);
            if (obj.user === this.state.user.id) {
                correctlist = obj.followers;
            }
        });

        //Update state accordingly
        if (correctlist === "") {
            this.setState({
                allFollowerslist: followinglist
            })
        }
        else {

            let liste2 = correctlist.split(":")
            let followers = liste2[0].split(",")
            let followings = liste2[1].split(",")

            this.setState({
                listedFollowers: followers[0] === "" ? [] : followers,
                listedFollowing: followings[0] === "" ? [] : followings,
                numberOfFollowers: (followers[0] === "" || followers[0] === ":") ? 0 : followers.length,
                numberOfFollowing: (followings[0] === "" || followings[0] === ":") ? 0 : followings.length,
                allFollowerslist: followinglist
            })

        }

    }

    unfollow = async (item) => { // Stop following item(another users username) and remove this user as their follower 
        let thisUsersIndex = -1;
        let otherUsersIndex = -1;

        // Get all users
        const brukerheaders = new Headers();
        brukerheaders.append("Content-Type", "application/json");
        brukerheaders.append("Authorization", `Token ${localStorage.getItem("token")}`);

        var parameters = {
            method: "GET",
            headers: brukerheaders,
            redirect: "follow"
        };

        const brukerresponse = await fetch(
            "http://localhost:8000/users/",
            parameters
        );
        const brukerdata = await brukerresponse.json();

        // Find the other users id from their username
        let otherusersId = -1
        brukerdata.map(obj => {
            if (obj.username === item) {
                otherusersId = obj.id
            }
        });

        // Find the position of this user and the other user in the list over all users followers:following
        var i;
        for (i = 0; i < this.state.allFollowerslist.length; i++) {
            if (this.state.allFollowerslist[i].user === otherusersId) {
                otherUsersIndex = i;
            }
            else if (this.state.allFollowerslist[i].user === this.state.user.id) {
                thisUsersIndex = i;
            }
        }

        //Unfollowing item(other user) from this user:
        let thisuserslist = this.state.allFollowerslist[thisUsersIndex];
        await this.handleDelete(thisuserslist);

        let currentfollowerslist = ""
        let newFollowing = []
        for (const watcher of this.state.listedFollowing) {
            if (watcher !== item) {
                currentfollowerslist += watcher + ","
                newFollowing.push(watcher);
            }
        }
        let splitIndex = thisuserslist.followers.indexOf(":");
        currentfollowerslist = thisuserslist.followers.substring(0, splitIndex + 1) + currentfollowerslist.substring(0, currentfollowerslist.length - 1);

        this.setState({
            listedFollowing: newFollowing,
            numberOfFollowing: newFollowing.length,
        })

        let tmprow = {
            user: this.state.user.id,
            followers: currentfollowerslist
        }
        this.updateDB(tmprow)

        //Removing this user at one of item's(other user's) followers:
        let otheruserslist = this.state.allFollowerslist[otherUsersIndex];
        await this.handleDelete(otheruserslist);

        let othersFollowers = otheruserslist.followers.split(":")[0].split(",");
        currentfollowerslist = "";
        for (const username of othersFollowers) {
            if (username !== this.state.user.username) {
                currentfollowerslist = username + ","
            }
        }
        splitIndex = otheruserslist.followers.indexOf(":");
        currentfollowerslist = currentfollowerslist.substring(0, currentfollowerslist.length - 1) + otheruserslist.followers.substring(splitIndex);

        tmprow = {
            user: otheruserslist.user,
            followers: currentfollowerslist
        }
        await this.updateDB(tmprow);

        this.refreshList();
    };

    updateDB = async (userFollowersList) => { //Post a row in the followers-table in database
        const headers = new Headers();
        headers.append("Content-Type", "application/json");
        headers.append("Authorization", `Token ${localStorage.getItem("token")}`);

        var parameters = {
            method: "POST",
            headers: headers,
            body: JSON.stringify(userFollowersList),
            redirect: "follow"
        };
        await fetch(`http://localhost:8000/followers/`, parameters);
    };

    handleDelete = async (userFollowersList) => { //Deleting a users row in followers-table in database
        const headers = new Headers();
        headers.append("Content-Type", "application/json");
        headers.append("Authorization", `Token ${localStorage.getItem("token")}`);

        var parameters = {
            method: "DELETE",
            headers: headers,
            body: JSON.stringify(userFollowersList),
            redirect: "follow"
        };

        await fetch(
            `http://localhost:8000/followers/${userFollowersList.user}/`,
            parameters
        );
    };

    refreshList = async () => { //Update the list over all users Followers:followings
        const headers = new Headers();
        headers.append("Content-Type", "application/json");
        headers.append("Authorization", `Token ${localStorage.getItem("token")}`);

        var parameters = {
            method: "GET",
            headers: headers,
            redirect: "follow"
        };
        const response = await fetch(
            "http://localhost:8000/followers/",
            parameters
        );
        const data = await response.json();

        let liste = [];
        data.map(obj => {
            liste.push(obj);
        });

        this.setState({ allFollowerslist: liste });
    };

    renderFollowing = () => { //Render the username of the users this user is following as li's
        if (this.state.listedFollowing.length === 0) {
            return (
                <li>
                    <span>Not following any watchers yet :(</span>
                </li>
            );
        } else {
            return this.state.listedFollowing.map(item => (
                <li>{item}
                    <button
                        onClick={() => this.unfollow(item)}
                        className="btn btn-danger btnstyle4"
                    >
                        X
                    </button>
                </li>
            ));
        }
    };

    renderFollowers = () => { //Render the username of this users followers as li's
        if (this.state.listedFollowers.length === 0) {
            return (
                <li>
                    <span>No followers yet :(</span>
                </li>
            );
        } else {
            return this.state.listedFollowers.map(item => (
                <li>{item}</li>
            ));
        }
    };

    handleSearch = () => { // If let's search is clicked, send user to searchpage
        this.props.history.push(`/searchpage/`);
    };

    render() {
        return (
            < div >
                {/*Search text and button*/}
                <div className="searchstyle1">
                    <div className="inputformat3">
                        <h5>Looking for Watchers?</h5>
                        <div>
                            <button
                                className="btn btn-success btnstyle1"
                                onClick={() => this.handleSearch()}
                            >
                                Let's search!
                            </button>
                        </div>
                    </div>
                </div>

                {/*Profilebox*/}
                <div className="profileStyle">
                    <h2>{this.state.user.username}</h2>
                    <div className="padstyle">
                        <img
                            src={require("./defaultProfilePic.png")}
                            className="profilePicStyle"
                        ></img>
                        <div className="currentlyWatchingStyle">
                            {" "}
                            Currently watching: {this.state.currentlyWatching}
                        </div>
                    </div>
                    <div className="padstyle">
                        <div className="filmbox">
                            Films{" "}
                            <Link className="navword1" to="/watchlist">
                                {this.state.numberOfFilms}
                            </Link>
                        </div>
                        <div className="followingbox">
                            Following{" "}
                            <Link className="navword1" to="/followers">
                                {this.state.numberOfFollowing}
                            </Link>
                        </div>
                        <div className="followerbox">
                            Followers{" "}
                            <Link className="navword1" to="/followers">
                                {this.state.numberOfFollowers}
                            </Link>
                        </div>
                    </div>
                </div>

                {/*Listed Followers*/}
                <div className="ulcontainer">
                    <h3>{this.state.user.username}'s Followers</h3>
                    <br />
                    <ul className="ulstyle">{this.renderFollowers()}</ul>
                </div>
                <br />

                {/*Listed Following*/}
                <div className="ulcontainer">
                    <h3>{this.state.user.username} Follows</h3>
                    <br />
                    <ul className="ulstyle">{this.renderFollowing()}</ul>
                </div>
            </div >
        );
    }
}

export default FollowerComponent;
