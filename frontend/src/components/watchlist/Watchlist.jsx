import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import AuthenticatedRoute from "./AuthenticatedRoute.jsx";
import LoginComponent from "./LoginComponent.jsx";
import FilmListComponent from "./FilmListComponent.jsx";
import HeaderComponent from "./HeaderComponent.jsx";
import LogoutComponent from "./LogoutComponent.jsx";
import WelcomeComponent from "./WelcomeComponent.jsx";
import FooterComponent from "./FooterComponent.jsx";
import ErrorComponent from "./ErrorComponent.jsx";
import ProfileComponent from "./ProfileComponent.jsx";
import SignUpComponent from "./SignUpComponent.jsx";
import SearchpageComponent from "./SearchpageComponent.jsx";
import FollowersComponent from "./FollowersComponent.jsx";


//Main component for all the different pages of the site
class Watchlist extends Component {
  render() {
    return (
      <div className="Watchlist">
        <Router>
          <HeaderComponent></HeaderComponent>
          <Switch>
            <Route path="/" exact component={LoginComponent}></Route>
            <Route path="/login" component={LoginComponent}></Route>
            <AuthenticatedRoute
              path="/welcome/"
              component={WelcomeComponent}
            ></AuthenticatedRoute>
            <AuthenticatedRoute
              path="/watchlist/"
              component={FilmListComponent}
            ></AuthenticatedRoute>
            <AuthenticatedRoute
              path="/logout"
              component={LogoutComponent}
            ></AuthenticatedRoute>
            <Route path="/signup" component={SignUpComponent}></Route>
            <Route path="/profile" component={ProfileComponent}></Route>
            <Route path="/searchpage" component={SearchpageComponent}></Route>
            <Route path="/followers" component={FollowersComponent}></Route>
            <Route component={ErrorComponent}></Route>
          </Switch>
          <FooterComponent></FooterComponent>
        </Router>
      </div>
    );
  }
}

export default Watchlist;
