import React, { Component } from "react";
import { getUser } from "./userAuth";

class SearchpageComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: "",
      allusers: [],
      allwatchlists: [],
      search: "",
      fittingUsers: [],
      allFollowingPairs: [],
      usersFollowing: []
    };
  }

  async componentDidMount() {

    //Get all users
    const brukerheaders = new Headers();
    brukerheaders.append("Content-Type", "application/json");
    brukerheaders.append("Authorization", `Token ${localStorage.getItem("token")}`);

    var parameters = {
      method: "GET",
      headers: brukerheaders,
      redirect: "follow"
    };

    const brukerresponse = await fetch(
      "http://localhost:8000/users/",
      parameters
    );
    const brukerdata = await brukerresponse.json();

    // Get this users userdata
    const token = localStorage.getItem("token");
    const userdata = await getUser(token);

    // List all users with the extra attribute "open" used to determine whether to show their watchlist
    let brukerliste = [];
    brukerdata.map(obj => {
      if (obj.username !== userdata.username) {
        let w = {
          id: obj.id,
          username: obj.username,
          email: obj.email,
          open: false,
        }
        brukerliste.push(w);
      }
    });

    brukerliste.shift(); //Remove admin from the list


    //Get all watchlists
    const headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Authorization", `Token ${localStorage.getItem("token")}`);

    var parameters = {
      method: "GET",
      headers: headers,
      redirect: "follow"
    };

    const response = await fetch(
      "http://localhost:8000/watchlist/",
      parameters
    );
    const filmlistedata = await response.json();

    let filmliste = []; //Create a list of all user's watchlists
    filmlistedata.map(obj => {
      filmliste.push(obj);
    });

    //Get all followers:followings 
    const headers2 = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Authorization", `Token ${localStorage.getItem("token")}`);

    var parameters = {
      method: "GET",
      headers: headers2,
      redirect: "follow"
    };

    const response2 = await fetch(
      "http://localhost:8000/followers/",
      parameters
    );
    const followingdata = await response2.json();

    let followinglist = []; //List all users followers:following (Followers and following on the format: username,username,username...)
    let thisusersfollowing = []; //List of the people this user is already following, used to determine wither to how the "follow" button
    followingdata.map(obj => {
      followinglist.push(obj);
      if (obj.user === userdata.id) {
        let ind = obj.followers.indexOf(":");
        if (ind >= 0 && ind < obj.followers.length - 1) {
          thisusersfollowing = obj.followers.split(":")[1].split(",");
        }
      }
    });

    //Update state accordingly
    this.setState({
      user: userdata,
      allusers: brukerliste,
      allwatchlists: filmliste,
      allFollowingPairs: followinglist,
      usersFollowing: thisusersfollowing
    });
  }

  updateInput(key, value) { //Keeping state up to date with inputfields
    this.setState({
      [key]: value
    });
  }

  handleSearch = () => { //If search-button clicked
    let liste1 = [] // List all users with usernames that match with the search
    for (const watcher of this.state.allusers) {
      if (watcher.username.includes(this.state.search)) {
        liste1.push(watcher)
      }
    }
    this.setState({ fittingUsers: liste1 })
  };

  togglePanel = (item) => {
    //Show or hide item's(a user) watchlist by updating the "open" attribute of item 
    //in the lists allusers and fittingusers accordlingly
    let tmp1 = [...this.state.allusers]
    let tmp2 = [...this.state.fittingUsers]
    var correctIndex1 = this.state.allusers.indexOf(item)

    tmp1[correctIndex1] = {
      id: item.id,
      username: item.username,
      email: item.email,
      open: !item.open,
    }
    if (tmp2.length > 0) {
      var correctIndex2 = this.state.fittingUsers.indexOf(item)
      if (correctIndex2 > -1) {
        tmp2[correctIndex2] = {
          id: item.id,
          username: item.username,
          email: item.email,
          open: !item.open,
        }
        this.setState({
          allusers: tmp1,
          fittingUsers: tmp2
        })
      }
      this.setState({
        allusers: tmp1
      })
    }
    else {
      this.setState({
        allusers: tmp1
      })
    }
  }

  handleFollow = async (watcher) => { //If follow button clicked
    let thisUsersIndex = -1;
    let otherUsersIndex = -1;

    //Find the index of this user and the other user(watcher) in the list over all users' followers:followings
    var i;
    for (i = 0; i < this.state.allFollowingPairs.length; i++) {
      if (this.state.allFollowingPairs[i].user === watcher.id) {
        otherUsersIndex = i;
      }
      else if (this.state.allFollowingPairs[i].user === this.state.user.id) {
        thisUsersIndex = i;
      }
    }

    let addedFollowerList = [...this.state.usersFollowing]
    addedFollowerList.push(watcher.username)

    //Add watcher(a users username) to the list of users this user is following
    this.setState({
      usersFollowing: addedFollowerList
    })

    if (thisUsersIndex === -1) { //This user does not have a followers:following list in the DB
      // Post a new row in the DB for this user with the other user as someone this user is following
      let tmprow = {
        user: this.state.user.id,
        followers: ":" + watcher.username
      }
      await this.updateDB(tmprow);
    }
    else { //This user already has a followers:following list in the DB
      //Get this user's list over followers:following(a string) from the DB
      let thisuserslist = this.state.allFollowingPairs[thisUsersIndex];

      // Delete their row from the DB
      await this.handleDelete(thisuserslist);

      // Add the other user to the following side of this user
      let currentfollowerslist = ""
      if (thisuserslist.followers.indexOf(":") === thisuserslist.followers.length - 1) {
        currentfollowerslist = thisuserslist.followers + watcher.username;
      }
      else {
        currentfollowerslist = thisuserslist.followers + "," + watcher.username;
      }

      // Post a new row in the DB for this user with their following list updated(the followers string updated)
      let tmprow = {
        user: this.state.user.id,
        followers: currentfollowerslist
      }
      await this.updateDB(tmprow);
    }

    if (otherUsersIndex === -1) { //The other user does not have a followers:following list in the DB
      // Post a new row in the DB for the other user with this user following them
      let tmprow2 = {
        user: watcher.id,
        followers: this.state.user.username + ":"
      }
      await this.updateDB(tmprow2);
    }
    else { //The other user already has a followers:following list in the DB
      //Get the other user's list over followers:following(a string) from the DB
      let otheruserslist = this.state.allFollowingPairs[otherUsersIndex];

      // Delete their row from the DB
      await this.handleDelete(otheruserslist);

      // Add this user to the followers side of the other user
      let currentfollowerslist = otheruserslist.followers;
      let followersFollowingSplitIndex = currentfollowerslist.indexOf(':');
      if (currentfollowerslist.substring(0, followersFollowingSplitIndex) === "") {
        currentfollowerslist = currentfollowerslist.substring(0, followersFollowingSplitIndex) + this.state.user.username + currentfollowerslist.substring(followersFollowingSplitIndex);
      }
      else {
        currentfollowerslist = currentfollowerslist.substring(0, followersFollowingSplitIndex) + "," + this.state.user.username + currentfollowerslist.substring(followersFollowingSplitIndex);
      }

      // Post a new row in the DB for the other user with theit followers list updated(the followers string updated)
      let tmprow2 = {
        user: watcher.id,
        followers: currentfollowerslist
      }
      await this.updateDB(tmprow2);
    }
    this.refreshList();
  }

  refreshList = async () => { //Update the list over all users Followers:followings
    const headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Authorization", `Token ${localStorage.getItem("token")}`);

    var parameters = {
      method: "GET",
      headers: headers,
      redirect: "follow"
    };
    const response = await fetch(
      "http://localhost:8000/followers/",
      parameters
    );
    const data = await response.json();

    let liste = [];
    data.map(obj => {
      liste.push(obj);
    });

    this.setState({ allFollowingPairs: liste });
  };

  updateDB = async (userFollowersList) => { //Post a row in the followers-table in database
    const headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Authorization", `Token ${localStorage.getItem("token")}`);

    var parameters = {
      method: "POST",
      headers: headers,
      body: JSON.stringify(userFollowersList),
      redirect: "follow"
    };
    await fetch(`http://localhost:8000/followers/`, parameters);
  };

  handleDelete = async (userFollowersList) => { //Deleting a users row in followers-table in database
    const headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Authorization", `Token ${localStorage.getItem("token")}`);

    var parameters = {
      method: "DELETE",
      headers: headers,
      body: JSON.stringify(userFollowersList),
      redirect: "follow"
    };

    await fetch(
      `http://localhost:8000/followers/${userFollowersList.user}/`,
      parameters
    );
  };

  renderMovies = (item) => { //Render the films in item's (a user) filmslist as li's
    let poslistString = "";
    for (const watcher of this.state.allwatchlists) {
      if (watcher.user === item.id) {
        poslistString = watcher.movies;
      }
    }
    if (poslistString === null || poslistString === "") {
      return (
        <li>
          <span>No watchlist yet :(</span>
        </li>
      );
    } else {
      let possibleList = poslistString.split(";");

      return possibleList.map(item => (
        <li>
          Film: {item.split("-")[0]}, Rating: {item.split("-")[1]}/5
        </li>
      ));
    }
  };

  renderItems = () => { //Render all the users that fit the search as li's
    return this.state.fittingUsers.map(item => (
      <li>
        <div className="padstyle">
          <h4>Watcher: {item.username}</h4>
          {!this.state.usersFollowing.includes(item.username) ? (
            <button
              onClick={() => this.handleFollow(item)}
              className="btn btnstyle6"
            >
              Follow
                </button>
          ) : null
          }
        </div>
        <button
          onClick={() => this.togglePanel(item)}
          className="btn btnstyle7"
        >
          Show/hide
          </button>
        {" "}{item.username}'s Watchlist
          {item.open ? (
          <div className="content">
            <ul className="ulwatchliststyle2">{this.renderMovies(item)}</ul>
          </div>
        ) : null
        }

      </li >
    ));
  };

  render() {
    return (
      <div>
        {/* Search button anf inputfield */}
        <div className="searchstyle">
          <div className="inputformat3">
            <input
              type="text"
              placeholder="New search"
              value={this.state.search}
              onChange={e => this.updateInput("search", e.target.value)}
            />
            <button
              className="btn btn-success btnstyle5"
              onClick={() => this.handleSearch()}
            >
              Go
            </button>
          </div>
        </div>

        {/* List all users that fit the search */}
        <h6 className="searchresults">Search results</h6>
        {this.state.fittingUsers.length === 0 ?
          <ul className="simpleulstyle">
            <li>
              <span>No matching Watchers :(</span>
            </li>
          </ul>
          :
          <div>
            <ul className="ulwatchliststyle3">{this.renderItems()}</ul>
          </div>
        }
      </div>
    );
  }
}

export default SearchpageComponent;
