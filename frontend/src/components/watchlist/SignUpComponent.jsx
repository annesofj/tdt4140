import React, { Component } from "react";
import "../../bootstrap.css";

import { registerUser } from "./userAuth";

class SignUpComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      email: "",
      password: "",
      repeatPassword: "",
      token: "",
      register: true,
      error: false,
      usedNames: [] //List over usernames that are already taken
      //loading: false,
    };

    this.onChange = this.onChange.bind(this);
  }

  async componentDidMount() {

    //Get all users
    const brukerheaders = new Headers();
    brukerheaders.append("Content-Type", "application/json");
    brukerheaders.append("Authorization", "Basic YWRtaW46cXdlcXdl");

    var parameters = {
      method: "GET",
      headers: brukerheaders,
      redirect: "follow"
    };

    const brukerresponse = await fetch(
      "http://localhost:8000/users/",
      parameters
    );
    const brukerdata = await brukerresponse.json();

    let brukerliste = []; //List of all taken usernames in the DB
    brukerdata.map(obj => {
      brukerliste.push(obj.username);
    });

    //Update the state accordingly
    this.setState({
      usedNames: brukerliste
    })
  }

  onChange(event) { //Keeping state up to date with inputfields
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  getUserFields() { //Get this users username, email and password
    return {
      username: `${this.state.username}`,
      email: `${this.state.email}`,
      password: `${this.state.password}`
    };
  }

  signupClicked() { // If submit button clicked
    if (this.validateSubmit()) {  // register the user if the input is valid
      registerUser(this.getUserFields()).then(() =>
        this.props.history.push(`/login/`)
      );
    } else {
      this.setState({
        error: true
      });
    }
  }

  validateSubmit() { // Check that all inputfields are valid
    if (this.state.register) {
      return (
        this.state.username.length > 2 &&
        this.state.password.length >= 3 &&
        this.state.password === this.state.repeatPassword &&
        this.state.email.length > 6 &&
        this.state.email.includes("@"));
    } else {
      return this.state.username.length > 2 && this.state.password.length > 4;
    }
  }

  render() {
    return (
      <div className="signupstyle">
        <h1>Sign up</h1>
        {this.state.error &&
          (<div className="alert alert-warning">
            Invalid Fields
            {this.state.usedNames.includes(this.state.username) ?
              <div>
                <p className="errortextstyle">
                  Username already taken, please try another
              </p>
              </div>
              :
              <div>
                <p className="errortextstyle">
                  Username must be at least 3 characters,
              </p>
                <p className="errortextstyle">
                  password must be at least 4 characters,
              </p>
                <p className="errortextstyle">
                  email must include '@' and be at least 6 characters.
              </p>
              </div>
            }
          </div>
          )}
        <div className="loginstyleuserName">
          Username{" "}
          <div className="inputformat">
            <input
              className="form-group"
              type="username"
              name="username"
              placeholder="Username"
              value={this.state.username}
              onChange={this.onChange}
              required
            />
          </div>
        </div>

        <div className="loginstyleuserName">
          E-mail{" "}
          <div className="inputformat">
            <input
              type="email"
              name="email"
              placeholder="E-mail"
              value={this.state.email}
              onChange={this.onChange}
              required
            />
          </div>
        </div>

        <div className="loginstylePassword">
          Password{" "}
          <div className="inputformat">
            <input
              type="password"
              name="password"
              placeholder="Password"
              value={this.state.password}
              onChange={this.onChange}
              required
            />
          </div>
        </div>

        <div className="loginstylePassword">
          Repeat password{" "}
          <div className="inputformat">
            <input
              type="password"
              name="repeatPassword"
              placeholder="Repeat password"
              value={this.state.repeatPassword}
              onChange={this.onChange}
              required
            />
          </div>
        </div>

        <button
          className="btn btn-success btnstyle1"
          onClick={() => this.signupClicked()}
          type="submit"
        >
          Register
        </button>
      </div>
    );
  }
}

export default SignUpComponent;
