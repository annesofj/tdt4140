import React, { Component } from "react";
import AuthenticationService from "./AuthenticationService.js";
import { Link } from "react-router-dom";
import { withRouter } from "react-router";


// Component for the header/navbar, present on all the site's pages 
// Note that most of the links will only be visible if the user is logged in (isUserLoggedIn = true)
class HeaderComponent extends Component {
  render() {
    const isUserLoggedIn = AuthenticationService.isUserLoggedIn();

    return (
      <header>
        <nav className="navbar navbar-expand-md navstyle">
          <div className="logostyle">
            <Link className="navbar-brand" to={"/welcome/"}>
              <img src={require("./logoWL.svg")} className="logo1"></img>
            </Link>
          </div>

          <ul className="navbar-nav">
            {isUserLoggedIn && (
              <li>
                <Link className="nav-link navword1" to="/profile">
                  Profile
                </Link>
              </li>
            )}

            {isUserLoggedIn && (
              <li>
                <Link className="nav-link navword1" to="/welcome/jens">
                  Home
                </Link>
              </li>
            )}

            {isUserLoggedIn && (
              <li>
                <Link className="nav-link navword1" to="/watchlist">
                  Watchlist
                </Link>
              </li>
            )}

            {isUserLoggedIn && (
              <li>
                <Link className="nav-link navword1" to="/followers">
                  Followers
                </Link>
              </li>
            )}
          </ul>

          <ul className="navbar-nav navbar-collapse justify-content-end">
            {!isUserLoggedIn && (
              <li>
                <Link className="nav-link navword1" to="/signup">
                  Sign up
                </Link>
              </li>
            )}

            {!isUserLoggedIn && (
              <li>
                <Link className="nav-link navword1" to="/login">
                  Log in
                </Link>
              </li>
            )}

            {isUserLoggedIn && (
              <li>
                <Link
                  className="nav-link navword1"
                  to="/logout"
                  onClick={AuthenticationService.logout}
                >
                  Logout
                </Link>
              </li>
            )}
          </ul>
        </nav>
        <hr></hr>
      </header>
    );
  }
}

export default withRouter(HeaderComponent);
