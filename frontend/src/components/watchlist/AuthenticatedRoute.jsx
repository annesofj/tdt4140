import React, { Component } from 'react'
import AuthenticationService from "./AuthenticationService.js";
import { Route, Redirect } from 'react-router-dom';

//Check whether a user is logged so they may enter a specific page
class AuthenticatedRoute extends Component {
    render() {
        if (AuthenticationService.isUserLoggedIn()) {
            return <Route {...this.props}></Route>
        }
        else {
            return <Redirect to="/login"></Redirect>
        }
    }
}

export default AuthenticatedRoute