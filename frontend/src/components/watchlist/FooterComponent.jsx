import React, { Component } from "react";


// Component for the footer, present on all the site's pages
class FooterComponent extends Component {
  render() {
    return (
      <div>
        <hr></hr>
        <footer className="footer">
          <span className="text-muted">
            Copyright &copy; 2020, WatchList AS
          </span>
        </footer>
      </div>
    );
  }
}

export default FooterComponent;
