import React, { Component } from "react";
import { Link } from "react-router-dom";

import { loginUser } from "./userAuth";

class LoginComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      hasLoginFailed: false,
      showSuccessMessage: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.loginClicked = this.loginClicked.bind(this);
  }

  handleChange(event) { //Keep state consistent with updates in inputfields
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  getUserFields() { //Get this user's username and password
    return {
      username: `${this.state.username}`,
      password: `${this.state.password}`
    };
  }

  routeToWelcome = user => {// Route the user to the home page(Welcome page)
    if (user.username) {
      this.setState({
        hasLoginFailed: true,
        loading: false,
        showSuccessMessage: false
      });
      this.props.history.push(`/welcome/${user.username}`);
    }
  };

  saveToken = token => { // Saves the token in localstorage for the rest of this users session
    localStorage.setItem("token", token.token);
    return true;
  };

  async loginClicked() { // If login button clicked
    const token = await loginUser(this.getUserFields()); //Check if the user exists (a token will be returned)
    // Log the user in if the username and password corresponds to a user in the database (a token was returned)
    if (token) {
      this.saveToken(token);
      this.setState({
        hasLoginFailed: false,
        loading: true,
        showSuccessMessage: true
      });
      this.props.history.push(`/welcome/`);
    } else {
      this.setState({
        hasLoginFailed: true,
        loading: false,
        showSuccessMessage: false
      });
    }
  }

  render() {
    return (
      <div className="loginstyle">
        <h1>Log in</h1>
        <div className="container"></div>
        {this.state.hasLoginFailed && (
          <div className="alert alert-warning">Invalid Credentials</div>
        )}
        {this.state.showSuccessMessage && (
          <div className="alert alert-success">Login Successful</div>
        )}

        <div className="loginstyleuserName">
          Username{" "}
          <div className="inputformat">
            <input
              type="text"
              name="username"
              value={this.state.username}
              onChange={this.handleChange}
              placeholder="Username"
            />
          </div>
        </div>

        <div className="loginstylePassword">
          Password{" "}
          <div className="inputformat">
            <input
              type="password"
              name="password"
              value={this.state.password}
              onChange={this.handleChange}
              placeholder="Password"
            />
          </div>
        </div>

        <div>
          <button
            className="btn btn-success btnstyle1"
            onClick={this.loginClicked}
          >
            Log in
          </button>
        </div>
        <br />
        <div className="newWatcherSyle">
          Not a watcher? Sign up{" "}
          <Link className="navword1" to="/signup">
            {" "}
            here.
          </Link>
        </div>
      </div>
    );
  }
}

export default LoginComponent;
