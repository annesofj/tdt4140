import React from 'react'
import { Link } from "react-router-dom";

//Errorpage for bad urls 
function ErrorComponent() {
  return (
    <div>
      An Error Occurred. Click <Link to="/login">here</Link> to go back to login
      page.
      </div>
  );
}

export default ErrorComponent