import React, { Component } from "react";
import { Link } from "react-router-dom";
import { getUser } from "./userAuth";

class ProfileComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: "",
      currentlyWatching: "None",
      profilePic: "./defaultProfilePic.png",
      listedMovies: "", //String with this users the movies and ratings
      numberOfFilms: 0,
      numberOfFollowers: 0,
      numberOfFollowing: 0
    };
  }

  async componentDidMount() {

    //Get all watchlists
    const headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Authorization", `Token ${localStorage.getItem("token")}`);

    var parameters = {
      method: "GET",
      headers: headers,
      redirect: "follow"
    };

    const response = await fetch(
      "http://localhost:8000/watchlist/",
      parameters
    );
    const data = await response.json();

    let liste = [];
    data.map(obj => {
      liste.push(obj);
    });

    // Get this users userdata
    const token = localStorage.getItem("token");
    const userdata = await getUser(token);
    this.setState({ user: userdata });

    //Find this user in the watchlist and set the number of films accordingly
    for (const el of liste) {
      if (el.user === userdata.id) {
        this.setState({ listedMovies: el.movies });
        if (el.movies === null) {
          this.setState({ numberOfFilms: 0 });
        } else {
          const totalFilms = el.movies.split(";");
          this.setState({ numberOfFilms: totalFilms.length });
        }
      }
    }

    //Get all followers:followings 
    const headers2 = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Authorization", `Token ${localStorage.getItem("token")}`);

    var parameters = {
      method: "GET",
      headers: headers2,
      redirect: "follow"
    };

    const response2 = await fetch(
      "http://localhost:8000/followers/",
      parameters
    );
    const followingdata = await response2.json();

    // Add all users follower:followings to followinglist, and find this users followers:following
    let followinglist = [];
    let correctlist = ""
    followingdata.map(obj => {
      followinglist.push(obj);
      if (obj.user === this.state.user.id) {
        correctlist = obj.followers;
      }
    });

    //Update state accordingly
    if (correctlist !== "") {
      let liste2 = correctlist.split(":")
      let followers = liste2[0].split(",")
      let followings = liste2[1].split(",")

      this.setState({
        numberOfFollowers: (followers[0] === "" || followers[0] === ":") ? 0 : followers.length,
        numberOfFollowing: (followings[0] === "" || followings[0] === ":") ? 0 : followings.length,
      })
    }
  }

  renderItems = () => { //Render the films in the filmslist as li's
    let poslistString = this.state.listedMovies;

    if (poslistString === null || poslistString === "") {
      return (
        <li>
          <span>No watchlist yet :(</span>
        </li>
      );
    } else {
      let possibleList = poslistString.split(";");

      return possibleList.map(item => (
        <li>
          Film: {item.split("-")[0]}, Rating: {item.split("-")[1]}/5
    </li>
      ));
    }
  };

  render() {
    return (
      <div>
        {/*Profilebox*/}
        <div className="profileStyle">
          <h2>{this.state.user.username}</h2>
          <div className="padstyle">
            <img
              src={require("./defaultProfilePic.png")}
              className="profilePicStyle"
            ></img>
            <div className="currentlyWatchingStyle">
              {" "}
              Currently watching: {this.state.currentlyWatching}
            </div>
          </div>
          <div className="padstyle">
            <div className="filmbox">
              Films{" "}
              <Link className="navword1" to="/watchlist">
                {this.state.numberOfFilms}
              </Link>
            </div>
            <div className="followingbox">
              Following{" "}
              <Link className="navword1" to="/followers">
                {this.state.numberOfFollowing}
              </Link>
            </div>
            <div className="followerbox">
              Followers{" "}
              <Link className="navword1" to="/followers">
                {this.state.numberOfFollowers}
              </Link>
            </div>
          </div>
        </div>
        {/*User's watchlist (listed films)*/}
        <div className="ulcontainer">
          <h3>{this.state.user.username}'s Watchlist</h3>
          <br />
          <ul className="ulstyle">{this.renderItems()}</ul>
        </div>
      </div>
    );
  }
}

export default ProfileComponent;
