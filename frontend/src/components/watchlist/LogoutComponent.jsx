import React, { Component } from 'react'

//Component for when the user logges out
class LogoutComponent extends Component {
  render() {
    return (
      <div>
        <h1>You are now logged out. See you soon!</h1>
        <div className="container">WATCHLIST RULES</div>
      </div>
    );
  }
}

export default LogoutComponent