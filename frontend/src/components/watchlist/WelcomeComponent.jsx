import React, { Component } from "react";
import { Link } from "react-router-dom";
import { getUser } from "./userAuth";

class WelcomeComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: "",
      currentlyWatching: "None",
      profilePic: "./defaultProfilePic.png",
      listedMovies: "",
      numberOfFilms: 0,
      numberOfFollowers: 0,
      numberOfFollowing: 0,
      following: [],  //Usernames of the users this user if following
      search: "",
      activity: [] //Feedelements of the users this user is following
    };
  }

  async componentDidMount() {

    //Get all watchlists
    const headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Authorization", `Token ${localStorage.getItem("token")}`);

    var parameters = {
      method: "GET",
      headers: headers,
      redirect: "follow"
    };

    const response = await fetch(
      "http://localhost:8000/watchlist/",
      parameters
    );
    const data = await response.json();

    let liste = [];
    data.map(obj => {
      liste.push(obj);
    });

    // Get this users userdata
    const token = localStorage.getItem("token");
    const userdata = await getUser(token);
    this.setState({ user: userdata });

    //Find this user in the watchlist and set the number of films accordingly
    for (const el of liste) {
      if (el.user === userdata.id) {
        this.setState({ listedMovies: el.movies });
        const totalFilms = el.movies.split(";");
        this.setState({ numberOfFilms: totalFilms.length });
      }
    }

    //Get all followers:followings
    const headers2 = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Authorization", `Token ${localStorage.getItem("token")}`);

    var parameters = {
      method: "GET",
      headers: headers2,
      redirect: "follow"
    };

    const response2 = await fetch(
      "http://localhost:8000/followers/",
      parameters
    );
    const followingdata = await response2.json();

    let followinglist = []; // Add all users follower:followings to followinglist
    let correctlist = ""; // Find this users followers:following
    followingdata.map(obj => {
      followinglist.push(obj);
      if (obj.user === this.state.user.id) {
        correctlist = obj.followers;
      }
    });

    let followings = []; //List of usernames that this user follows
    if (correctlist !== "") {
      let liste2 = correctlist.split(":")
      let followers = liste2[0].split(",")
      followings = liste2[1].split(",")

      // Update the state accordingly
      this.setState({
        numberOfFollowers: (followers[0] === "" || followers[0] === ":") ? 0 : followers.length,
        numberOfFollowing: (followings[0] === "" || followings[0] === ":") ? 0 : followings.length,
        followings: followings[0] === "" ? [] : followings
      })
    }

    // Get all feedlements
    const headers4 = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Authorization", `Token ${localStorage.getItem("token")}`);

    var parameters = {
      method: "GET",
      headers: headers4,
      redirect: "follow"
    };

    const response4 = await fetch(
      "http://localhost:8000/feedelement/",
      parameters
    );
    const feeddata = await response4.json();

    let feedlist = []; // List of the feedelements created by users that this user follows
    feeddata.map(obj => {
      if (followings.includes(obj.username)) {
        feedlist.push(obj)
      }
    });

    feedlist.reverse() // Let the newest added elements show up at the top

    // Update the state accordingly
    this.setState({
      activity: feedlist
    })

  }

  updateInput(key, value) { //Keeping state up to date with inputfields
    this.setState({
      [key]: value
    });
  }

  handleSearch = () => { // If let's search is clicked, send user to searchpage
    this.props.history.push(`/searchpage/`);
  };

  renderItems = () => { //Render all the feedelements in the acitvity list as li's
    return this.state.activity.map(item => (
      <li>
        <h7 className="feedwatcher2">Update:</h7>
        <br />
        <h7 className="feedwatcher">{item.username}</h7>
        {" "}added "{item.addedMovie.split("-")[0]}" to their watchlist
          with the rating {item.addedMovie.split("-")[1]}/5.
      </li >
    ));
  };

  render() {
    return (
      <div>
        {/*Search text and button*/}
        <div className="searchstyle1">
          <div className="inputformat3">
            <h5>Looking for Watchers?</h5>
            <div>
              <button
                className="btn btn-success btnstyle1"
                onClick={() => this.handleSearch()}
              >
                Let's search!
            </button>
            </div>
          </div>
        </div>

        {/*Profilebox*/}
        <div className="profileStyle">
          <h2>{this.state.user.username}</h2>
          <div className="padstyle">
            <img
              src={require("./defaultProfilePic.png")}
              className="profilePicStyle"
            ></img>
            <div className="currentlyWatchingStyle">
              {" "}
              Currently watching: {this.state.currentlyWatching}
            </div>
          </div>
          <div className="padstyle">
            <div className="filmbox">
              Films{" "}
              <Link className="navword1" to="/watchlist">
                {this.state.numberOfFilms}
              </Link>
            </div>
            <div className="followingbox">
              Following{" "}
              <Link className="navword1" to="/followers">
                {this.state.numberOfFollowing}
              </Link>
            </div>
            <div className="followerbox">
              Followers{" "}
              <Link className="navword1" to="/followers">
                {this.state.numberOfFollowers}
              </Link>
            </div>
          </div>
        </div>

        {/*List the relevant feedelements(activity)*/}
        {this.state.activity.length === 0 ?
          <ul className="simpleulstyle2">
            <li>
              <span>No feed to show yet, follow</span>
            </li>
            <li>Watchers to get updates! :)</li>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
          </ul>
          :
          <div>
            <ul className="feedstyle">{this.renderItems()}</ul>
          </div>
        }
      </div>
    );
  }
}

export default WelcomeComponent;



