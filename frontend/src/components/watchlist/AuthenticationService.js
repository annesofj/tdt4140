//Handles log in and log out
class AuthenticationService {
    registerSuccessfulLogin(username, password) {
        console.log("registerSuccessfulLogin");
    }

    logout() {
        localStorage.removeItem('token');
    }

    isUserLoggedIn() {
        let user = localStorage.getItem('token');
        if (user === null) return
        return true
    }
}

export default new AuthenticationService()