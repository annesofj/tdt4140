import React, { Component } from "react";
//import axios from "axios";
import { getUser } from "./userAuth";

//Page where a user may create and edit their watchlist
class FilmListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newFilm: "",
      DbWatchlist: [],  //Watchlist of all users
      username: "",
      DbWatchlistEl: {   // Watchlist of this user
        user: "",
        movies: null
      },
      alreadyHasList: false,
      validFilm: true
    };
    this.componentDidMount = this.componentDidMount.bind(this);
    this.updateInput = this.updateInput.bind(this);
  }

  async componentDidMount() {
    this.refreshList();

    const token = localStorage.getItem("token"); // Get this users id and username, update state
    await getUser(token).then(data => {
      this.setState({
        username: data.username,
        DbWatchlistEl: {
          user: parseInt(data.id, 10),
          movies: this.state.DbWatchlistEl.movies
        }
      });
    });

    this.getCurrentFilmlist();
  }

  getCurrentFilmlist = () => { //Get this users filmlist and update state (DbWatchlistEl)
    for (const el of this.state.DbWatchlist) {
      if (parseInt(el.user) === this.state.DbWatchlistEl.user) {
        this.setState({
          DbWatchlistEl: {
            user: this.state.DbWatchlistEl.user,
            movies: el.movies
          }
        });
        if (el.movies !== null) {
          this.setState({ alreadyHasList: true });
        }
      }
    }
  };

  refreshList = async () => { //Check for updates in watchlists, to make sure DBWatchlist is up tp date
    const headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Authorization", `Token ${localStorage.getItem("token")}`);

    var parameters = {
      method: "GET",
      headers: headers,
      redirect: "follow"
    };
    const response = await fetch(
      "http://localhost:8000/watchlist/",
      parameters
    );
    const data = await response.json();

    let liste = [];
    data.map(obj => {
      liste.push(obj);
    });

    this.setState({ DbWatchlist: liste });
  };

  renderItems = () => { //Render the films in the filmslist as li's
    let poslistString = this.state.DbWatchlistEl.movies;

    if (poslistString === null || poslistString === "") {
      return (
        <li>
          <span>No watchlist yet :(</span>
        </li>
      );
    } else {
      let possibleList = poslistString.split(";");

      return possibleList.map(item => (
        <li>
          Film: {item.split("-")[0]}, Rating: {item.split("-")[1]}/5
          <button
            onClick={() => this.removeFilm(item)}
            className="btn btn-danger btnstyle4"
          >
            X
          </button>
        </li>
      ));
    }
  };

  handleChange = async () => { //Handle update after movie has been added
    await this.updateDB();
    this.refreshList();
    this.setState({ newFilm: "" });
  };

  updateDB = async () => { //Update the user's watchlist in database
    if (this.state.alreadyHasList) { //Delete row if it already exists
      await this.handleDelete();
    }

    const headers = new Headers(); // Post the users updated watchlist
    headers.append("Content-Type", "application/json");
    headers.append("Authorization", `Token ${localStorage.getItem("token")}`);

    var parameters = {
      method: "POST",
      headers: headers,
      body: JSON.stringify(this.state.DbWatchlistEl),
      redirect: "follow"
    };

    await fetch(`http://localhost:8000/watchlist/`, parameters);
  };

  handleDelete = async () => { //Delete user's watchlist from the database
    const headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Authorization", `Token ${localStorage.getItem("token")}`);

    var parameters = {
      method: "DELETE",
      headers: headers,
      body: JSON.stringify(this.state.DbWatchlistEl),
      redirect: "follow"
    };

    await fetch(
      `http://localhost:8000/watchlist/${this.state.DbWatchlistEl.user}/`,
      parameters
    );
  };

  removeFilm = item => { //If user clicks on remove buttion
    // Remove film from filmlist
    let newString = "";
    const tempList = this.state.DbWatchlistEl.movies.split(";");
    for (const film of tempList) {
      if (film !== item) {
        newString = newString + film + ";";
      }
    }
    // Update state
    this.setState(
      {
        DbWatchlistEl: {
          user: this.state.DbWatchlistEl.user,
          movies: newString.slice(0, newString.length - 1)
        }
      },
      this.handleChange //update the database accordingly
    );
  };

  validateSubmit = () => { //Check that the user has entered a valid film on format film-rating
    if (this.state.newFilm.includes("-")) {
      const liste = this.state.newFilm.split("-");
      if (liste[0].length > 0 && liste[1].length > 0) {
        if (!isNaN(liste[1])) { // isNAN returns true if it is NOT a number
          const number = parseInt(liste[1]);
          if (number <= 5 && number >= 0) {
            return true;
          }
        }
      }
    }
    return false;
  };

  addFilm = async () => { //If users clicks add button
    if (this.validateSubmit()) { // Updates state if the input is valid
      this.setState({
        alreadyHasList: true,
        validFilm: true
      });

      // Update users watchlist ("movies" in state) with new film from newFilm field 
      if (this.state.DbWatchlistEl.movies === null) {
        this.setState(
          {
            DbWatchlistEl: {
              user: this.state.DbWatchlistEl.user,
              movies: this.state.newFilm
            }
          },
          this.handleChange
        );
      } else {
        this.setState(
          {
            DbWatchlistEl: {
              user: this.state.DbWatchlistEl.user,
              movies:
                this.state.DbWatchlistEl.movies + ";" + this.state.newFilm
            }
          },
          this.handleChange
        );
      }

      // Create a new feedelement of the users added movie: 
      const headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("Authorization", `Token ${localStorage.getItem("token")}`);

      const tmpfeedel = {
        username: this.state.username,
        addedMovie: this.state.newFilm
      }

      var parameters = {
        method: "POST",
        headers: headers,
        body: JSON.stringify(tmpfeedel),
        redirect: "follow"
      };

      await fetch(`http://localhost:8000/feedelement/`, parameters);


    } else {
      this.setState({
        validFilm: false
      });
    }
  };

  updateInput(key, value) { //Keeping state up to date with inputfields
    this.setState({
      [key]: value
    });
  }

  render() {
    return (
      <div>
        <div className="watchliststyle">
          {!this.state.validFilm && (
            <div className="alert alert-warning">
              Invalid Movie
              <p className="errortextstyle">Please use the format:</p>
              <p className="errortextstyle">"movie-rating"</p>
              <p className="errortextstyle">where the rating is between 0-5.</p>
            </div>
          )}
          Add a movie!
          <br />
          <div className="inputformat2">
            <input
              type="text"
              placeholder="Movie-Rating"
              value={this.state.newFilm}
              onChange={e => this.updateInput("newFilm", e.target.value)}
            />
          </div>
          <button
            className="btn btn-success btnstyle2"
            onClick={() => this.addFilm()}
          >
            Add
          </button>
        </div>
        <div className="ulcontainer1">
          <h3>{this.state.username}'s Watchlist</h3>
          <br />
          <ul className="ulwatchliststyle">{this.renderItems()}</ul>
        </div>
      </div>
    );
  }
}

export default FilmListComponent;
